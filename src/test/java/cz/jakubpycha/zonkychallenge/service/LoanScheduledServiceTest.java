package cz.jakubpycha.zonkychallenge.service;

import cz.jakubpycha.zonkychallenge.entity.Loan;
import cz.jakubpycha.zonkychallenge.zonkyclient.dto.LoanDto;
import cz.jakubpycha.zonkychallenge.zonkyclient.helper.Filter;
import cz.jakubpycha.zonkychallenge.zonkyclient.helper.FilterOperation;
import cz.jakubpycha.zonkychallenge.zonkyclient.helper.Sorting;
import cz.jakubpycha.zonkychallenge.zonkyclient.service.ZonkyApiService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class LoanScheduledServiceTest {

    @Test
    public void readLoansFromMarketplaceAndSaveThemLoanInDBTest() throws ZonkyApiService.ZonkyApiCallException {
        final LoanService loanService = mock(LoanService.class);
        final var lastPublishedDate = OffsetDateTime.now().minusDays(30);
        final Optional<Loan> loanOptional = Optional.of(createLoan(2345L, lastPublishedDate));
        when(loanService.getMostRecentlyByDatePublished())
                .thenReturn(loanOptional);

        final ZonkyApiService zonkyApiService = mock(ZonkyApiService.class);
        final List<LoanDto> zonkyApiResp = Arrays.asList(
                createLoanDto(123L, OffsetDateTime.now().minusDays(5)),
                createLoanDto(456L, OffsetDateTime.now().minusDays(2)),
                createLoanDto(789L, OffsetDateTime.now())
        );
        when(zonkyApiService.getMarketplaceLoans(any(Collection.class), any(List.class)))
                .thenReturn(zonkyApiResp);

        final LoanScheduledService scheduledService = new LoanScheduledServiceImpl(zonkyApiService, loanService);
        scheduledService.readLoansFromMarketplaceAndSaveThem();

        final ArgumentCaptor<List<Filter>> filterArgumentCaptor = ArgumentCaptor.forClass(List.class);
        final ArgumentCaptor<List<Sorting>> sortingArgumentCaptor = ArgumentCaptor.forClass(List.class);
        verify(zonkyApiService).getMarketplaceLoans(filterArgumentCaptor.capture(), sortingArgumentCaptor.capture());

        final ArgumentCaptor<Collection<LoanDto>> saveArgumentCaptor = ArgumentCaptor.forClass(Collection.class);
        verify(loanService).saveAll(saveArgumentCaptor.capture());

        Assertions.assertIterableEquals(zonkyApiResp, saveArgumentCaptor.getValue());
        Assertions.assertIterableEquals(
                Arrays.asList(new Filter(
                        LoanDto.FilterFields.DATE_PUBLISHED,
                        FilterOperation.GT,
                        lastPublishedDate.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME))
                ),
                filterArgumentCaptor.getValue());
        Assertions.assertIterableEquals(
                Arrays.asList(new Sorting(LoanDto.SortFields.DATE_PUBLISHED)),
                sortingArgumentCaptor.getValue()
        );

    }

    @Test
    public void readLoansFromMarketplaceAndSaveThemLoanNotInDBTest() throws ZonkyApiService.ZonkyApiCallException {
        final LoanService loanService = mock(LoanService.class);

        when(loanService.getMostRecentlyByDatePublished())
                .thenReturn(Optional.empty());

        final ZonkyApiService zonkyApiService = mock(ZonkyApiService.class);

        when(zonkyApiService.getMarketplaceLoans(anyCollection(), anyList()))
                .thenReturn(Arrays.asList(
                        createLoanDto(123L, OffsetDateTime.now().minusDays(5)),
                        createLoanDto(456L, OffsetDateTime.now())
                ));

        final LoanScheduledService loanScheduledService = new LoanScheduledServiceImpl(zonkyApiService, loanService);
        loanScheduledService.readLoansFromMarketplaceAndSaveThem();

        final ArgumentCaptor<Collection<Filter>> filterCaptor = ArgumentCaptor.forClass(Collection.class);
        final ArgumentCaptor<List<Sorting>> sortingCaptor = ArgumentCaptor.forClass(List.class);
        verify(zonkyApiService, times(1)).getMarketplaceLoans(filterCaptor.capture(), sortingCaptor.capture());

        Assertions.assertIterableEquals(Collections.emptyList(), filterCaptor.getValue());
        Assertions.assertIterableEquals(
                Arrays.asList(new Sorting(LoanDto.SortFields.DATE_PUBLISHED)),
                sortingCaptor.getValue()
        );
    }

    private LoanDto createLoanDto(Long id, OffsetDateTime datePublished) {
        final LoanDto loanDto = new LoanDto();
        loanDto.setId(id);
        loanDto.setDatePublished(datePublished);
        return loanDto;
    }

    private Loan createLoan(Long id, OffsetDateTime datePublished) {
        final Loan loan = new Loan();
        loan.setId(id);
        loan.setDatePublished(datePublished);
        return loan;
    }
}
