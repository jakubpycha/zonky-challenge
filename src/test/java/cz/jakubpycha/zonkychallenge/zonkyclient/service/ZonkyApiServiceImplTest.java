package cz.jakubpycha.zonkychallenge.zonkyclient.service;

import cz.jakubpycha.zonkychallenge.zonkyclient.ZonkyClientConfiguration;
import cz.jakubpycha.zonkychallenge.zonkyclient.dto.LoanDto;
import cz.jakubpycha.zonkychallenge.zonkyclient.helper.Filter;
import cz.jakubpycha.zonkychallenge.zonkyclient.helper.FilterOperation;
import cz.jakubpycha.zonkychallenge.zonkyclient.helper.Sorting;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ZonkyApiServiceImplTest {

    private static final String API_MOCK_URL = "https://zonky.test";

    @Test
    public void marketplaceLoansMockPagingTest() throws ZonkyApiService.ZonkyApiCallException {
        final RestTemplate restTemplate = mock(RestTemplate.class);
        final HttpHeaders responseHeaders = createResponseHeaders(5);

        final LoanDto loanDto1 = createLoan(1L);
        final LoanDto loanDto2 = createLoan(2L);
        final LoanDto loanDto3 = createLoan(3L);
        final LoanDto loanDto4 = createLoan(4L);
        final LoanDto loanDto5 = createLoan(5L);

        final ResponseEntity<List<LoanDto>> responseEntity1 = new ResponseEntity<>(asList(loanDto1, loanDto2, loanDto3), responseHeaders, HttpStatus.OK);
        final ResponseEntity<List<LoanDto>> responseEntity2 = new ResponseEntity<>(asList(loanDto4, loanDto5), responseHeaders, HttpStatus.OK);

        when(restTemplate.exchange(any(URI.class), any(HttpMethod.class), any(HttpEntity.class),
                any(ParameterizedTypeReference.class)))
                .thenReturn(responseEntity1)
                .thenReturn(responseEntity2);

        final var zonkyClientConfiguration = new ZonkyClientConfiguration();
        zonkyClientConfiguration.setDefaultPageSize(3);
        zonkyClientConfiguration.setUrl(API_MOCK_URL);
        final ZonkyApiService apiService = new ZonkyApiServiceImpl(restTemplate, zonkyClientConfiguration);

        final List<LoanDto> respLoanDtos = apiService.getMarketplaceLoans();

        assertEquals(asList(loanDto1, loanDto2, loanDto3, loanDto4, loanDto5), respLoanDtos);
    }

    @Test
    public void marketplaceLoansMockOrderAndFilterTest() throws ZonkyApiService.ZonkyApiCallException {
        final RestTemplate restTemplate = mock(RestTemplate.class);

        final HttpHeaders responseHeaders = createResponseHeaders(0);

        final ResponseEntity<List<LoanDto>> responseEntity = new ResponseEntity<>(Collections.emptyList(), responseHeaders, HttpStatus.OK);

        when(restTemplate.exchange(any(URI.class), any(HttpMethod.class), any(HttpEntity.class),
                any(ParameterizedTypeReference.class)))
                .thenReturn(responseEntity);

        final ZonkyClientConfiguration zonkyClientConfiguration = createZonkyClientConfiguration();

        final ZonkyApiService zonkyApiService = new ZonkyApiServiceImpl(restTemplate, zonkyClientConfiguration);

        final String actualTimeString = ZonedDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);

        zonkyApiService.getMarketplaceLoans(
                asList(
                        new Filter(LoanDto.FilterFields.DATE_PUBLISHED, FilterOperation.GT, actualTimeString),
                        new Filter(LoanDto.FilterFields.RATING, FilterOperation.EQ, "A")
                ),
                asList(
                        new Sorting(LoanDto.SortFields.DATE_PUBLISHED, true),
                        new Sorting(LoanDto.SortFields.RATING)
                )
        );

        final ArgumentCaptor<HttpEntity> argumentCaptor = ArgumentCaptor.forClass(HttpEntity.class);

        final var expectedURI = URI.create(String.format(
                "%s/loans/marketplace?datePublished__gt=%s&rating__eq=%s",
                API_MOCK_URL,
                actualTimeString,
                "A"));

        verify(restTemplate).exchange(eq(expectedURI), eq(HttpMethod.GET), argumentCaptor.capture(),
                any(ParameterizedTypeReference.class));

        final HttpHeaders actualHeaders = argumentCaptor.getValue().getHeaders();

        assertEquals("-datePublished,rating", actualHeaders.getFirst("x-order"));
        assertEquals("0", actualHeaders.getFirst("x-page"));
        assertEquals(zonkyClientConfiguration.getDefaultPageSize().toString(), actualHeaders.getFirst("x-size"));
        assertEquals(zonkyClientConfiguration.getUserAgent(), actualHeaders.getFirst(HttpHeaders.USER_AGENT));
    }


    @Test
    void marketplaceLoansMockBadRequestTest() {
        final RestTemplate restTemplate = mock(RestTemplate.class);

        final ResponseEntity<List<LoanDto>> responseEntity = new ResponseEntity<>(Collections.emptyList(),
                createResponseHeaders(0), HttpStatus.BAD_REQUEST);

        when(restTemplate.exchange(any(URI.class), any(HttpMethod.class), any(HttpEntity.class),
                any(ParameterizedTypeReference.class)))
                .thenReturn(responseEntity);

        final ZonkyApiService zonkyApiService = new ZonkyApiServiceImpl(restTemplate, createZonkyClientConfiguration());

        assertThrows(ZonkyApiService.ZonkyApiCallException.class, zonkyApiService::getMarketplaceLoans);

    }

    @Test
    void marketplaceLoansMockRestClientExceptionTest() {
        final RestTemplate restTemplate = mock(RestTemplate.class);

        when(restTemplate.exchange(any(URI.class), any(HttpMethod.class), any(HttpEntity.class),
                any(ParameterizedTypeReference.class)))
                .thenThrow(new RestClientException("Test exception"));

        final ZonkyApiService zonkyApiService = new ZonkyApiServiceImpl(restTemplate, createZonkyClientConfiguration());

        assertThrows(ZonkyApiService.ZonkyApiCallException.class, zonkyApiService::getMarketplaceLoans);
    }

    @NotNull
    private ZonkyClientConfiguration createZonkyClientConfiguration() {
        final ZonkyClientConfiguration zonkyClientConfiguration = new ZonkyClientConfiguration();
        zonkyClientConfiguration.setUrl(API_MOCK_URL);
        return zonkyClientConfiguration;
    }

    private HttpHeaders createResponseHeaders(Integer responseTotalSize) {
        final HttpHeaders headers = new HttpHeaders();
        headers.set("x-total", responseTotalSize.toString());
        return headers;
    }

    private LoanDto createLoan(Long id) {
        final LoanDto loanDto = new LoanDto();
        loanDto.setId(id);
        loanDto.setDatePublished(OffsetDateTime.now());
        return loanDto;
    }

}
