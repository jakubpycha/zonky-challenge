package cz.jakubpycha.zonkychallenge.service;

public interface LoanScheduledService {

    /**
     * Read loans from Zonky marketplace API and save them to our DB.
     * Loans are loaded  incrementally.
     */
    void readLoansFromMarketplaceAndSaveThem();

}
