package cz.jakubpycha.zonkychallenge.service;

import cz.jakubpycha.zonkychallenge.zonkyclient.dto.LoanDto;
import cz.jakubpycha.zonkychallenge.zonkyclient.helper.Filter;
import cz.jakubpycha.zonkychallenge.zonkyclient.helper.FilterOperation;
import cz.jakubpycha.zonkychallenge.zonkyclient.helper.Sorting;
import cz.jakubpycha.zonkychallenge.zonkyclient.service.ZonkyApiService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Service
@Log4j2
public class LoanScheduledServiceImpl implements LoanScheduledService {

    private final ZonkyApiService zonkyApiService;
    private final LoanService loanService;

    @Autowired
    public LoanScheduledServiceImpl(ZonkyApiService zonkyApiService, LoanService loanService) {
        this.zonkyApiService = Objects.requireNonNull(zonkyApiService);
        this.loanService = Objects.requireNonNull(loanService);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Scheduled(fixedDelayString = "${zonkyChallenge.readLoansDelay:300000}")
    public void readLoansFromMarketplaceAndSaveThem() {
        log.info("Scheduled loan reading from Zonky marketplace started");
        final List<Filter> filters = new ArrayList<>();
        this.loanService.getMostRecentlyByDatePublished()
                .ifPresent(loan -> filters.add(new Filter(
                        LoanDto.FilterFields.DATE_PUBLISHED,
                        FilterOperation.GT,
                        loan.getDatePublished().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME))
                ));
        try {
            final List<LoanDto> marketplaceLoans = this.zonkyApiService
                    .getMarketplaceLoans(filters, Collections.singletonList(new Sorting(LoanDto.SortFields.DATE_PUBLISHED)));
            this.loanService.saveAll(marketplaceLoans);
        } catch (ZonkyApiService.ZonkyApiCallException e) {
            log.error(
                    "Scheduled loans reading from Zonky marketplace failed. We will try again in the next scheduled task.",
                    e);
        }
    }
}
