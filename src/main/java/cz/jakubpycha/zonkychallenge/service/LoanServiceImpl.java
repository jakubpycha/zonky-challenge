package cz.jakubpycha.zonkychallenge.service;

import cz.jakubpycha.zonkychallenge.entity.Loan;
import cz.jakubpycha.zonkychallenge.graphql.dto.LoanOrderByInput;
import cz.jakubpycha.zonkychallenge.mapper.DtoToEntityMapper;
import cz.jakubpycha.zonkychallenge.mapper.LoanDtoToEntityMapper;
import cz.jakubpycha.zonkychallenge.repository.LoanRepository;
import cz.jakubpycha.zonkychallenge.zonkyclient.dto.LoanDto;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Log4j2
public class LoanServiceImpl implements LoanService {

    private final LoanRepository loanRepository;

    private final DtoToEntityMapper<LoanDto, Loan> loanDtoToEntityMapper;

    @Autowired
    public LoanServiceImpl(LoanRepository loanRepository, LoanDtoToEntityMapper loanDtoToEntityMapper) {
        this.loanRepository = Objects.requireNonNull(loanRepository);
        this.loanDtoToEntityMapper = Objects.requireNonNull(loanDtoToEntityMapper);
    }

    /**
     * {@inheritDoc}
     */
    @Transactional(readOnly = true)
    @Override
    public Optional<Loan> getMostRecentlyByDatePublished() {
        return loanRepository.findTopByOrderByDatePublishedDesc();
    }

    /**
     * {@inheritDoc}
     */
    @Transactional
    @Override
    public void saveAll(Collection<LoanDto> loanDtos) {
        log.debug(String
                .format(
                        "Saving loans to DB: %s",
                        loanDtos.stream().map(LoanDto::toString).collect(Collectors.joining())
                )
        );
        loanRepository.saveAll(loanDtos.stream()
                .map(this.loanDtoToEntityMapper::convertDtoToEntity)
                .collect(Collectors.toList())
        );
    }

    /**
     * {@inheritDoc}
     */
    @Transactional(readOnly = true)
    @Override
    public Page<Loan> getLoans(Integer page, Integer size, LoanOrderByInput orderBy) {
        log.debug(String.format("Get loans page: %s, size: %s, order: %s", page, size, orderBy));
        final Sort sort;
        if (orderBy != null) {
            Sort tempSort = Sort.by(orderBy.getColumnName());
            if (orderBy.isDesc()) {
                sort = tempSort.descending();
            } else {
                sort = tempSort.ascending();
            }
        } else {
            sort = Sort.unsorted();
        }
        return loanRepository.findAll(PageRequest.of(page, size, sort));
    }

    /**
     * {@inheritDoc}
     */
    @Transactional(readOnly = true)
    @Override
    public Optional<Loan> getLoan(Long id) {
        return loanRepository.findById(id);
    }
}
