package cz.jakubpycha.zonkychallenge.service;

import cz.jakubpycha.zonkychallenge.entity.Loan;
import cz.jakubpycha.zonkychallenge.graphql.dto.LoanOrderByInput;
import cz.jakubpycha.zonkychallenge.zonkyclient.dto.LoanDto;
import org.springframework.data.domain.Page;

import java.util.Collection;
import java.util.Optional;

/**
 * Interface for {@link Loan} service operations
 */
public interface LoanService {

    /**
     * Returns the newest {@link Loan} by published date
     *
     * @return the newest {@link Loan}
     */
    Optional<Loan> getMostRecentlyByDatePublished();

    /**
     * Returns paginated result of all {@link Loan} ordered by {@link LoanOrderByInput}
     *
     * @param page    number of page
     * @param size    size of page
     * @param orderBy by which column is sorted
     */
    Page<Loan> getLoans(Integer page, Integer size, LoanOrderByInput orderBy);

    /**
     * Returns single {@link Loan} by id
     *
     * @param id
     */
    Optional<Loan> getLoan(Long id);

    /**
     * Save all provided {@link LoanDto} from argument.
     * @param loanDtos
     */
    void saveAll(Collection<LoanDto> loanDtos);

}
