package cz.jakubpycha.zonkychallenge.mapper;

import cz.jakubpycha.zonkychallenge.entity.Loan;
import cz.jakubpycha.zonkychallenge.zonkyclient.dto.LoanDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class LoanDtoToEntityMapper extends DtoToEntityMapper<LoanDto, Loan> {

    final ModelMapper modelMapper = new ModelMapper();
    private Loan loanEntity;


    /**
     * {@inheritDoc}
     */
    @Override
    public Loan convertDtoToEntity(LoanDto dto) {
        loanEntity = modelMapper.map(dto, Loan.class);
        return loanEntity;
    }

    @Override
    protected void configure() {
        skip(source.getInsuranceHistory());
        skip(source.getPhotos());
    }
}
