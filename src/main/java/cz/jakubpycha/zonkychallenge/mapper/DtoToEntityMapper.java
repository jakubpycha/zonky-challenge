package cz.jakubpycha.zonkychallenge.mapper;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

/**
 * Abstract class which is contract for DTO to entity conversions
 *
 * @param <S> source DTO
 * @param <D> target entity
 */
public abstract class DtoToEntityMapper<S, D> extends PropertyMap<S, D> {

    final ModelMapper modelMapper = new ModelMapper();

    /**
     * Converts DTO {@link S} to entity {@link D}
     *
     * @param dto input for conversion
     * @return target entity
     */
    public abstract D convertDtoToEntity(S dto);
}
