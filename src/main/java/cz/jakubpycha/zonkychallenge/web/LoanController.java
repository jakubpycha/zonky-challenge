package cz.jakubpycha.zonkychallenge.web;

import cz.jakubpycha.zonkychallenge.graphql.dto.LoanOrderByInput;
import cz.jakubpycha.zonkychallenge.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Objects;

@Controller
public class LoanController {

    private final LoanService loanService;

    @Autowired
    public LoanController(LoanService loanService) {
        this.loanService = Objects.requireNonNull(loanService);
    }

    @GetMapping("/loans")
    public String showLoans(Model model) {
        model.addAttribute("loans", this.loanService.getLoans(0, 300, LoanOrderByInput.datePublished_DESC));
        return "loans";
    }
}
