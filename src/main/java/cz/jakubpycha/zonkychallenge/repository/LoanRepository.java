package cz.jakubpycha.zonkychallenge.repository;

import cz.jakubpycha.zonkychallenge.entity.Loan;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LoanRepository extends PagingAndSortingRepository<Loan, Long> {

    Optional<Loan> findTopByOrderByDatePublishedDesc();

}
