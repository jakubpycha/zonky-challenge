package cz.jakubpycha.zonkychallenge.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Loan implements Serializable {

    @Id
    private Long id;

    @CreatedDate
    private LocalDateTime created;

    @LastModifiedDate
    private LocalDateTime updated;

    private String url;
    private String name;

    @Lob
    private String story;

    private String purpose;
    private String nickName;
    private Long termInMonths;

    @Column(scale = 4, precision = 5)
    private BigDecimal interestRate;

    @Column(scale = 4, precision = 5)
    private BigDecimal revenueRate;
    private BigDecimal annuityWithInsurance;
    private String rating;
    private Boolean topped;
    private BigDecimal amount;
    private String countryOfOrigin;
    private String currency;
    private BigDecimal remainingInvestment;
    private BigDecimal reservedAmount;

    @Column(scale = 4, precision = 5)
    private BigDecimal investmentRate;
    private Boolean covered;
    private OffsetDateTime datePublished;
    private Boolean published;
    private OffsetDateTime deadline;
    private Long investmentsCount;
    private Long questionsCount;
    private String region;
    private String mainIncomeType;
    private Boolean insuranceActive;

}
