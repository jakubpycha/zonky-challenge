package cz.jakubpycha.zonkychallenge.zonkyclient;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Data
@ConfigurationProperties(prefix = "zonky.client")
public class ZonkyClientConfiguration {

    /**
     * API base url
     */
    private String url;

    /**
     * Client user agent identification
     */
    private String userAgent = "zonky-challenge/0.0.1-SNAPSHOT ()";

    /**
     * Default paging size
     */
    private Integer defaultPageSize = 2000;

    /**
     * If from date published is not specified that now - this number of days is used
     */
    private Integer defaultSinceDatePublished = 10;


}
