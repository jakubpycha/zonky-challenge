package cz.jakubpycha.zonkychallenge.zonkyclient.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import cz.jakubpycha.zonkychallenge.zonkyclient.helper.FilterField;
import cz.jakubpycha.zonkychallenge.zonkyclient.helper.SortField;
import lombok.Data;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;

/**
 * Data transfer object for LoanDto entity from Zonky api
 */
@Data
public class LoanDto {

    private Long id;
    private String url;
    private String name;
    private String story;
    private String purpose;
    private List<PhotoDto> photos;
    private String nickName;
    private Long termInMonths;
    private BigDecimal interestRate;
    private BigDecimal revenueRate;
    private BigDecimal annuityWithInsurance;
    private String rating;
    private Boolean topped;
    private BigDecimal amount;
    private String countryOfOrigin;
    private String currency;
    private BigDecimal remainingInvestment;
    private BigDecimal reservedAmount;
    private BigDecimal investmentRate;
    private Boolean covered;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private OffsetDateTime datePublished;
    private Boolean published;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private OffsetDateTime deadline;
    private Long investmentsCount;
    private Long questionsCount;
    private String region;
    private String mainIncomeType;
    private Boolean insuranceActive;
    private List<InsuranceHistoryItemDto> insuranceHistory;

    public enum FilterFields implements FilterField {
        DATE_PUBLISHED("datePublished"), COVERED("covered"), RATING("rating"),
        TERM_IN_MONTHS("termInMonths");

        String name;

        FilterFields(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public enum SortFields implements SortField {
        DATE_PUBLISHED("datePublished"), COVERED("covered"), INCOMES("incomes"),
        RATING("rating"), TERM_IN_MONTHS("termInMonths");

        String name;

        SortFields(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

}
