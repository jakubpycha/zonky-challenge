package cz.jakubpycha.zonkychallenge.zonkyclient.dto;

import lombok.Data;

/**
 * DTO for PhotoDto entity from Zonky API
 */
@Data
class PhotoDto {

    private String name;
    private String url;

}
