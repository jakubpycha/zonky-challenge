package cz.jakubpycha.zonkychallenge.zonkyclient.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;

/**
 * DTO for InsuranceHistoryItemDto entity from Zonky API
 */
@Data
class InsuranceHistoryItemDto {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate policyPeriodFrom;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate policyPeriodTo;

}
