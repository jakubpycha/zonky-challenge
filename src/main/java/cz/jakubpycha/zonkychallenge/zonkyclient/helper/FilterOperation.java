package cz.jakubpycha.zonkychallenge.zonkyclient.helper;

public enum FilterOperation {
    CONTAINS, GT, GTE, EQ // and next
}
