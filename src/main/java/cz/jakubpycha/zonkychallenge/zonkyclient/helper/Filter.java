package cz.jakubpycha.zonkychallenge.zonkyclient.helper;

import java.util.Objects;

/**
 * Helper class for Zonky api filtering
 */
public class Filter {
    private final FilterField filterField;
    private final FilterOperation operation;
    private final String value;

    public Filter(FilterField field, FilterOperation operation, String value) {
        this.filterField = Objects.requireNonNull(field);
        this.operation = Objects.requireNonNull(operation);
        this.value = Objects.requireNonNull(value);
    }

    /**
     * Returns query param name for URL
     *
     * @return query param name
     */
    public String getQueryName() {
        return String.format("%s__%s", this.filterField.getName(), this.operation.name().toLowerCase());
    }

    /**
     * Returns query param value for URL
     *
     * @return query param value
     */
    public String getQueryValue() {
        return this.value;
    }

    public FilterField getFilterField() {
        return filterField;
    }

    @Override
    public String toString() {
        return String.format("%s=%s", this.getQueryName(), this.getQueryValue());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Filter filter = (Filter) o;
        return filterField.equals(filter.filterField) &&
                operation == filter.operation;
    }

    @Override
    public int hashCode() {
        return Objects.hash(filterField, operation);
    }
}
