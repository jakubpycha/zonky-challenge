package cz.jakubpycha.zonkychallenge.zonkyclient.helper;

public interface SortField {
    /**
     * Returns name of the field for API sorting
     */
    String getName();
}
