package cz.jakubpycha.zonkychallenge.zonkyclient.helper;

public interface FilterField {
    /**
     * Returns filter field name for api filtering
     */
    String getName();
}
