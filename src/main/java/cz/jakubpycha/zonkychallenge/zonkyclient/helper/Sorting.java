package cz.jakubpycha.zonkychallenge.zonkyclient.helper;

import java.util.Objects;

/**
 * Helper class for Zonky api sorting
 */
public class Sorting {
    private SortField sortField;
    private boolean descending = false;

    public Sorting(SortField sortField, boolean descending) {
        this.sortField = Objects.requireNonNull(sortField);
        this.descending = descending;
    }

    public Sorting(SortField sortField) {
        this.sortField = Objects.requireNonNull(sortField);
    }

    @Override
    public String toString() {
        return String.format("%s%s", this.descending ? "-" : "", this.sortField.getName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sorting sorting = (Sorting) o;
        return descending == sorting.descending &&
                sortField.equals(sorting.sortField);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sortField, descending);
    }
}
