package cz.jakubpycha.zonkychallenge.zonkyclient.service;

import cz.jakubpycha.zonkychallenge.zonkyclient.dto.LoanDto;
import cz.jakubpycha.zonkychallenge.zonkyclient.helper.Filter;
import cz.jakubpycha.zonkychallenge.zonkyclient.helper.Sorting;

import java.util.Collection;
import java.util.List;

/**
 * Contract definition for Zonky api operations
 */
public interface ZonkyApiService {

    /**
     * Loads loans data from Zonky Marketplace and returns {@link List} of {@link LoanDto}
     * If filter for datePublished is not specified then is used application settings.
     *
     * @param filters collection of filters for API.
     * @param sorting list of sorting values for API.
     * @return List of {@link LoanDto} or empty list
     */
    List<LoanDto> getMarketplaceLoans(Collection<Filter> filters, List<Sorting> sorting) throws ZonkyApiCallException;

    /**
     * Loads loans data from Zonky Marketplace and returns {@link List} of {@link LoanDto}
     * If filter for datePublished is not specified then is used application settings.
     *
     * @param filters Optional collection of filters for API
     * @return List of {@link LoanDto} or empty list
     */
    List<LoanDto> getMarketplaceLoans(Collection<Filter> filters) throws ZonkyApiCallException;

    /**
     * Loads loans data from Zonky Marketplace and returns {@link List} of {@link LoanDto}
     * Filter for date published is set by default from application settings.
     *
     * @param sorting list of sorting values for API
     * @return List of {@link LoanDto} or empty list
     */
    List<LoanDto> getMarketplaceLoans(List<Sorting> sorting) throws ZonkyApiCallException;


    /**
     * Loads loans data from Zonky Marketplace and returns {@link List} of {@link LoanDto}
     * Filter for date published is set by default from application settings.
     * By default sort date published.
     *
     * @return List of {@link LoanDto} or empty list
     */
    List<LoanDto> getMarketplaceLoans() throws ZonkyApiCallException;

    class ZonkyApiCallException extends Exception {

        public ZonkyApiCallException() {
        }

        public ZonkyApiCallException(String message) {
            super(message);
        }

        public ZonkyApiCallException(String message, Throwable cause) {
            super(message, cause);
        }
    }


}
