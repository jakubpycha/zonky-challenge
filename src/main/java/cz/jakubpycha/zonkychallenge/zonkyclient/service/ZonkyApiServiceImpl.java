package cz.jakubpycha.zonkychallenge.zonkyclient.service;

import cz.jakubpycha.zonkychallenge.zonkyclient.UriBuilderFactory;
import cz.jakubpycha.zonkychallenge.zonkyclient.ZonkyClientConfiguration;
import cz.jakubpycha.zonkychallenge.zonkyclient.dto.LoanDto;
import cz.jakubpycha.zonkychallenge.zonkyclient.helper.Filter;
import cz.jakubpycha.zonkychallenge.zonkyclient.helper.FilterOperation;
import cz.jakubpycha.zonkychallenge.zonkyclient.helper.Sorting;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Log4j2
public class ZonkyApiServiceImpl implements ZonkyApiService {

    private static final String LOANS_PATH = "/loans/marketplace";
    private static final String HEADER_SIZE = "X-Size";
    private static final String HEADER_TOTAL = "X-Total";
    private static final String HEADER_PAGE = "X-Page";
    private static final String HEADER_ORDER = "X-Order";
    private final RestTemplate restTemplate;
    private final ZonkyClientConfiguration configuration;

    @Autowired
    public ZonkyApiServiceImpl(RestTemplate restTemplate, ZonkyClientConfiguration zonkyClientConfiguration) {
        this.restTemplate = Objects.requireNonNull(restTemplate);
        this.configuration = Objects.requireNonNull(zonkyClientConfiguration);
    }

    @Override
    public List<LoanDto> getMarketplaceLoans(final Collection<Filter> filtersCollection, List<Sorting> sorting) throws ZonkyApiCallException {
        final List<Filter> filters = new ArrayList<>(filtersCollection);

        if (filters.stream().filter(filter -> filter.getFilterField().equals(LoanDto.FilterFields.DATE_PUBLISHED)).count() < 1) {
            final var defaultFromDate = ZonedDateTime.now().minusDays(this.configuration.getDefaultSinceDatePublished());
            final var fromDatePublishedFilter = new Filter(
                    LoanDto.FilterFields.DATE_PUBLISHED,
                    FilterOperation.GTE,
                    defaultFromDate.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)
            );
            filters.add(fromDatePublishedFilter);
        }

        final URI loansUri = this.createURI(this.configuration.getUrl() + LOANS_PATH, filters);
        final HttpHeaders headers = createHeaders(sorting);

        final List<LoanDto> result = new ArrayList<>();

        int page = 0;
        boolean allReaded = false;
        final var responseType = new ParameterizedTypeReference<List<LoanDto>>() {
        };
        while (!allReaded) {
            headers.set(HEADER_PAGE, Integer.toString(page));

            try {
                log.info(String.format("%s request to url: %s starting", HttpMethod.GET, loansUri));
                final var response = this.restTemplate.exchange(loansUri, HttpMethod.GET, new HttpEntity<>(headers), responseType);
                if (!response.getStatusCode().is2xxSuccessful()) {
                    // throw exception
                    throw new ZonkyApiCallException(String.format("Server returns code %s to %s request to url: %s",
                            response.getStatusCodeValue(), HttpMethod.GET, loansUri
                    ));
                }
                if (response.getBody() == null) {
                    throw new ZonkyApiCallException(
                            String.format("Server returns code %s to %s request to url: %s , but response body is null",
                                    response.getStatusCodeValue(), HttpMethod.GET, loansUri
                            )
                    );
                }
                result.addAll(response.getBody());
                int total = Integer.parseInt(Optional
                        .ofNullable(response.getHeaders().getFirst(HEADER_TOTAL)).orElse("0"));
                allReaded = result.size() == total;
                ++page;
            } catch (RestClientException e) {
                throw new ZonkyApiCallException(String.format("Unable to request url: %s with method %s",
                        loansUri, HttpMethod.GET), e);
            }
        }

        return result;
    }

    @Override
    public List<LoanDto> getMarketplaceLoans(Collection<Filter> filters) throws ZonkyApiCallException {
        return this.getMarketplaceLoans(filters, Collections.emptyList());
    }

    @Override
    public List<LoanDto> getMarketplaceLoans(List<Sorting> sorting) throws ZonkyApiCallException {
        return this.getMarketplaceLoans(Collections.emptyList(), sorting);
    }

    @Override
    public List<LoanDto> getMarketplaceLoans() throws ZonkyApiCallException {
        return this.getMarketplaceLoans(
                Collections.emptyList(),
                Collections.singletonList(new Sorting(LoanDto.SortFields.DATE_PUBLISHED))
        );
    }

    private HttpHeaders createHeaders(final List<Sorting> sortings) {
        final HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.USER_AGENT, this.configuration.getUserAgent());
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        if (sortings != null) {
            headers.set(HEADER_ORDER, sortings.stream().map(Sorting::toString).collect(Collectors.joining(",")));
        }
        headers.set(HEADER_SIZE, this.configuration.getDefaultPageSize().toString());
        return headers;
    }

    private URI createURI(final String requestUrl, final Collection<Filter> filters) {
        final UriComponentsBuilder uriComponentsBuilder = UriBuilderFactory.fromHttpUrl(requestUrl);

        if (filters != null) {
            filters
                    .forEach(filter -> {
                        uriComponentsBuilder.queryParam(filter.getQueryName(), filter.getQueryValue());
                    });
        }

        try {
            return new URI(uriComponentsBuilder.buildAndExpand().toUriString());
        } catch (URISyntaxException e) {
            log.error("Error in URI creation", e);
            return null;
        }
    }

}
