package cz.jakubpycha.zonkychallenge.zonkyclient;

import org.springframework.web.util.UriComponentsBuilder;

public class UriBuilderFactory extends UriComponentsBuilder {

    public UriBuilderFactory(UriComponentsBuilder other) {
        super(other);
    }

    public static UriComponentsBuilder fromHttpUrl(String httpUrl) {
        return new UriBuilderFactory(UriComponentsBuilder.fromHttpUrl(httpUrl));
    }

    // To be replaced by UriUtils.encodeUriVariables()
    private static Object[] encode(Object[] values) {
        Object[] encodedValues = new Object[values.length];

        for (int i = 0; i < values.length; i++) {
            if (values[i] != null) {
                encodedValues[i] = values[i].toString().replace("+", "%2B");
            }
        }

        return encodedValues;
    }

    @Override
    // queryParams(), replaceQueryParam() and replaceQueryParams() should be overridden and values should be processed in similar way
    public UriComponentsBuilder queryParam(String name, Object... values) {
        return super.queryParam(name, encode(values));
    }
}
