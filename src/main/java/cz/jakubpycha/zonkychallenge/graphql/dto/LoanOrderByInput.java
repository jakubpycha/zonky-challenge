package cz.jakubpycha.zonkychallenge.graphql.dto;

public enum LoanOrderByInput {
    datePublished_ASC("datePublished"), datePublished_DESC("datePublished", true),
    rating_ASC("rating"), rating_DESC("rating", true);

    String columnName;
    boolean desc;

    LoanOrderByInput(String columnName, boolean desc) {
        this.columnName = columnName;
        this.desc = desc;
    }

    LoanOrderByInput(String columnName) {
        this.columnName = columnName;
        this.desc = false;
    }

    public String getColumnName() {
        return columnName;
    }

    public boolean isDesc() {
        return desc;
    }
}
