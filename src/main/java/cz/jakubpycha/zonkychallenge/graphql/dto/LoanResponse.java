package cz.jakubpycha.zonkychallenge.graphql.dto;

import cz.jakubpycha.zonkychallenge.entity.Loan;
import lombok.Data;

import java.util.List;

@Data
public class LoanResponse {
    PageInfo pageInfo;
    List<Loan> data;
}
