package cz.jakubpycha.zonkychallenge.graphql.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import cz.jakubpycha.zonkychallenge.entity.Loan;
import cz.jakubpycha.zonkychallenge.graphql.dto.LoanOrderByInput;
import cz.jakubpycha.zonkychallenge.graphql.dto.LoanResponse;
import cz.jakubpycha.zonkychallenge.graphql.dto.PageInfo;
import cz.jakubpycha.zonkychallenge.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class LoanQueryResolver implements GraphQLQueryResolver {

    private final LoanService loanService;

    @Autowired
    public LoanQueryResolver(LoanService loanService) {
        this.loanService = Objects.requireNonNull(loanService);
    }

    public LoanResponse getLoans(Integer page, Integer size, LoanOrderByInput orderBy) {

        final var loans = loanService.getLoans(page, size, orderBy);
        final LoanResponse loanResponse = new LoanResponse();
        loanResponse.setPageInfo(PageInfo
                .builder()
                .totalCount(loans.getTotalElements())
                .totalPages(loans.getTotalPages())
                .build());
        loanResponse.setData(loans.getContent());
        return loanResponse;
    }

    public Loan getLoan(Long id) {
        return loanService.getLoan(id).orElse(null);
    }


}
