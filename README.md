# zonky-challenge

App for loans checking on Zonky marketplace.

Application reads data from **api.zonky.cz** and stores it in embedded H2 memory DB for further use.

For users provides two interfeces, 
one is graphql api on address `/graphql` and second one is simple web page on address `/loans` which shows latest 300 loans.

For better better interaction with api is available on `/grapiql` GraphiQl tool.



### Requirements

#### Development
 * JDK11
 * correct JAVA_HOME to JDK11
 * Docker for build Docker image
 
#### Production
 * Docker for running assembled image
 
## How to run

 1. First time build the docker image
 `docker build . --tag zonky-challenge`
 2. Finally run the image :smile: `docker run -p 8080:8080 zonky-challenge`
 
 ## How to dev
 
 Project use maven for dependencies and compilation. Required maven version is included in project.
 Next commands with maven goals are important for development.
 
 * Compilation `.\mvnw compile`
 * Test `.\mvnw test`
 * Run `.\mvnw spring-boot:run`
 * Target JAR `.\mvnw clean package`
 
 

## Todo

 * Tests for LoanServiceImpl, LoanDtoToEntityMapper, LoanQueryResolver
 * Filtering in loans graphql query
 * Logging
 * Web design
