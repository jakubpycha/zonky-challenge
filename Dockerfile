FROM maven:3-jdk-11 AS BUILDER
COPY . /usr/src/mymaven
WORKDIR /usr/src/mymaven
RUN mvn clean package

FROM openjdk:11
RUN mkdir -p /opt/zonkychallenge
WORKDIR /opt/zonkychallenge
COPY --from=BUILDER /usr/src/mymaven/target/zonky-challenge-*.jar ./zonky-challenge.jar

EXPOSE 8080
CMD ["java", "-jar", "zonky-challenge.jar"]
